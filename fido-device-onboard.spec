%global debug_package %{nil}
%define dracutlibdir %{_prefix}/lib/dracut
%bcond_without check
%global forgeurl https://github.com/fedora-iot/fido-device-onboard-rs

Version:        0.4.7

%forgemeta

Name:           fido-device-onboard
Release:        3%{?dist}
Summary:        A rust implementation of the FIDO Device Onboard Specification

# Apache-2.0
# Apache-2.0 OR BSL-1.0
# Apache-2.0 OR ISC OR MIT
# Apache-2.0 OR MIT
# (Apache-2.0 OR MIT) AND BSD-3-Clause
# Apache-2.0 WITH LLVM-exception OR Apache-2.0 OR MIT
# BSD-2-Clause
# BSD-3-Clause
# CC0-1.0
# CC0-1.0 OR Apache-2.0
# ISC
# MIT
# MIT OR Apache-2.0
# MIT OR Apache-2.0 OR Zlib
# MPL-2.0
# Unlicense OR MIT
# Zlib OR Apache-2.0 OR MIT

License:        Apache-2.0 and BSD and MIT
URL:            %{forgeurl}

Source0:        %{forgesource}
# this is a basic script to generate the vendor tarfile.
Source1:        make-vendored-tarfile.sh
%if "%{?commit}" != ""
Source2:        %{name}-rs-%{commit}-vendor-patched.tar.xz
%else
Source2:        %{name}-rs-%{version}-vendor-patched.tar.xz
%endif

ExclusiveArch:  %{rust_arches}
# Needs, at least, tss bindings regen
ExcludeArch:    s390x i686 %{power64}

%if 0%{?rhel} && !0%{?eln}
BuildRequires:  rust-toolset
%else
BuildRequires:  rust-packaging
%endif
BuildRequires:  clang-devel
BuildRequires:  cryptsetup-devel
BuildRequires:  golang
BuildRequires:  openssl-devel >= 3.0.1-12
BuildRequires:  systemd-rpm-macros
BuildRequires:  tpm2-tss-devel

%description
%{summary}.

%prep
%forgesetup
%cargo_prep

# Vendored crates
tar xf %{SOURCE2}
mkdir -p .cargo
cat >.cargo/config << EOF
[source.crates-io]
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "vendor"
EOF

%build
%cargo_build \
-F openssl-kdf/deny_custom

%install
install -D -m 0755 -t %{buildroot}%{_libexecdir}/fdo target/release/fdo-client-linuxapp
install -D -m 0755 -t %{buildroot}%{_libexecdir}/fdo target/release/fdo-manufacturing-client
install -D -m 0755 -t %{buildroot}%{_libexecdir}/fdo target/release/fdo-manufacturing-server
install -D -m 0755 -t %{buildroot}%{_libexecdir}/fdo target/release/fdo-owner-onboarding-server
install -D -m 0755 -t %{buildroot}%{_libexecdir}/fdo target/release/fdo-rendezvous-server
install -D -m 0755 -t %{buildroot}%{_libexecdir}/fdo target/release/fdo-serviceinfo-api-server
# duplicates as needed by AIO command
install -D -m 0755 -t %{buildroot}%{_libexecdir}/fdo target/release/fdo-owner-tool
install -D -m 0755 -t %{buildroot}%{_libexecdir}/fdo target/release/fdo-admin-tool
install -D -m 0755 -t %{buildroot}%{_bindir} target/release/fdo-owner-tool
install -D -m 0755 -t %{buildroot}%{_bindir} target/release/fdo-admin-tool
install -D -m 0644 -t %{buildroot}%{_unitdir} examples/systemd/*
install -D -m 0644 -t %{buildroot}%{_docdir}/fdo examples/config/*
mkdir -p %{buildroot}%{_sysconfdir}/fdo
# Dracut manufacturing service
install -D -m 0755 -t %{buildroot}%{dracutlibdir}/modules.d/52fdo dracut/52fdo/module-setup.sh
install -D -m 0755 -t %{buildroot}%{dracutlibdir}/modules.d/52fdo dracut/52fdo/manufacturing-client-generator
install -D -m 0755 -t %{buildroot}%{dracutlibdir}/modules.d/52fdo dracut/52fdo/manufacturing-client-service
install -D -m 0755 -t %{buildroot}%{dracutlibdir}/modules.d/52fdo dracut/52fdo/manufacturing-client.service

%package -n fdo-init
Summary: dracut module for device initialization
Requires: openssl-libs >= 3.0.1-12
%description -n fdo-init
%{summary}

%files -n fdo-init
%license LICENSE
%{dracutlibdir}/modules.d/52fdo/*
%{_libexecdir}/fdo/fdo-manufacturing-client

%package -n fdo-owner-onboarding-server
Summary: FDO Owner Onboarding Server implementation
Requires: openssl-libs >= 3.0.1-12
%description -n fdo-owner-onboarding-server
%{summary}

%files -n fdo-owner-onboarding-server
%license LICENSE
%{_libexecdir}/fdo/fdo-owner-onboarding-server
%{_libexecdir}/fdo/fdo-serviceinfo-api-server
%{_docdir}/fdo/serviceinfo-api-server.yml
%{_unitdir}/fdo-serviceinfo-api-server.service
%{_docdir}/fdo/owner-onboarding-server.yml
%{_unitdir}/fdo-owner-onboarding-server.service

%post -n fdo-owner-onboarding-server
%systemd_post fdo-owner-onboarding-server.service
%systemd_post fdo-serviceinfo-api-server.service

%preun -n fdo-owner-onboarding-server
%systemd_preun fdo-owner-onboarding-server.service
%systemd_post fdo-serviceinfo-api-server.service

%postun -n fdo-owner-onboarding-server
%systemd_postun_with_restart fdo-owner-onboarding-server.service
%systemd_postun_with_restart fdo-serviceinfo-api-server.service

%package -n fdo-rendezvous-server
Summary: FDO Rendezvous Server implementation
%description -n fdo-rendezvous-server
%{summary}

%files -n fdo-rendezvous-server
%license LICENSE
%{_libexecdir}/fdo/fdo-rendezvous-server
%{_docdir}/fdo/rendezvous-*.yml
%{_unitdir}/fdo-rendezvous-server.service

%post -n fdo-rendezvous-server
%systemd_post fdo-rendezvous-server.service

%preun -n fdo-rendezvous-server
%systemd_preun fdo-rendezvous-server.service

%postun -n fdo-rendezvous-server
%systemd_postun_with_restart fdo-rendezvous-server.service

%package -n fdo-manufacturing-server
Summary: FDO Manufacturing Server implementation
Requires: openssl-libs >= 3.0.1-12
%description -n fdo-manufacturing-server
%{summary}

%files -n fdo-manufacturing-server
%license LICENSE
%{_libexecdir}/fdo/fdo-manufacturing-server
%{_docdir}/fdo/manufacturing-server.yml
%{_unitdir}/fdo-manufacturing-server.service

%post -n fdo-manufacturing-server
%systemd_post fdo-manufacturing-server.service

%preun -n fdo-manufacturing-server
%systemd_preun fdo-manufacturing-server.service

%postun -n fdo-manufacturing-server
%systemd_postun_with_restart fdo-manufacturing-server.service

%package -n fdo-client
Summary: FDO Client implementation
Requires: openssl-libs >= 3.0.1-12
Requires: clevis
Requires: clevis-luks
Requires: cryptsetup
%description -n fdo-client
%{summary}

%files -n fdo-client
%license LICENSE
%{_libexecdir}/fdo/fdo-client-linuxapp
%{_unitdir}/fdo-client-linuxapp.service

%post -n fdo-client
%systemd_post fdo-client-linuxapp.service

%preun -n fdo-client
%systemd_preun fdo-client-linuxapp.service

%postun -n fdo-client
%systemd_postun_with_restart fdo-client-linuxapp.service

%package -n fdo-owner-cli
Summary: FDO Owner tools implementation
%description -n fdo-owner-cli
%{summary}

%files -n fdo-owner-cli
%license LICENSE
%{_bindir}/fdo-owner-tool
%{_libexecdir}/fdo/fdo-owner-tool

%package -n fdo-admin-cli
Summary: FDO admin tools implementation
Requires: fdo-manufacturing-server
Requires: fdo-init
Requires: fdo-client
Requires: fdo-rendezvous-server
Requires: fdo-owner-onboarding-server
Requires: fdo-owner-cli
%description -n fdo-admin-cli
%{summary}

%files -n fdo-admin-cli
%license LICENSE
%{_bindir}/fdo-admin-tool
%{_libexecdir}/fdo/fdo-admin-tool
%{_unitdir}/fdo-aio.service
%dir %{_sysconfdir}/fdo

%post -n fdo-admin-cli
%systemd_post fdo-aio.service

%preun -n fdo-admin-cli
%systemd_preun fdo-aio.service

%postun -n fdo-admin-cli
%systemd_postun_with_restart fdo-aio.service

%changelog
* Mon Feb 20 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 0.4.7-3
- Fix services start

* Wed Feb 15 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 0.4.7-2
- Upstream fix for rhbz#2168089

* Wed Nov 30 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 0.4.7-1
- Update to 0.4.7
- Package updates and cleanup

* Tue Mar 29 2022 Antonio Murdaca <runcom@linux.com> - 0.4.5-1
- bump to 0.4.5

* Mon Feb 28 2022 Antonio Murdaca <runcom@linux.com> - 0.4.0-2
- fix runtime requirements to use openssl-libs and not -devel

* Thu Feb 24 2022 Antonio Murdaca <runcom@linux.com> - 0.4.0-1
- upgrade to 0.4.0

* Tue Feb 01 2022 Antonio Murdaca <runcom@linux.com> - 0.3.0-1
- bump to 0.3.0

* Tue Jan 11 2022 Antonio Murdaca <runcom@linux.com> - 0.2.0-2
- use patched vendor w/o win files and rename license

* Mon Dec 13 2021 Antonio Murdaca <runcom@linux.com> - 0.2.0-1
- import fido-device-onboard
